import React, { Component } from 'react';
import './App.css';

interface IState {
    cellBoard: any[],
    isNextGeneration: boolean,
    initialCellActive: any[],
    nextGen: any[];
}

interface IProps {}

class App extends Component<IProps, IState>{

    private interval: any;

    constructor(props: IProps){
        super(props);

        this.state = {
            cellBoard: [],
            initialCellActive: [],
            isNextGeneration: false,
            nextGen: [],
        }
    }

    clickGeneration(){
        if(this.state.initialCellActive.length < 1){
            alert("Please make at least one cell alive");
            return false;
        }

        this.setState(prevState => ({
            isNextGeneration: !prevState.isNextGeneration,
        }), () => {

            if(this.state.isNextGeneration){
                // add timer to auto-create cell evolution
                this.interval = setInterval(() => this.evolveCells(), 200);
            } else {
                // clear timer when stopped
                clearInterval(this.interval);
            }
        });
    }

    resetGeneration(){
        // stop timer and clear state variables
        clearInterval(this.interval);

        this.setState({
            initialCellActive: [],
            isNextGeneration: false,
            nextGen: [],
        });
    }


    clickCell(x: number, y: number){
        
        if(this.state.isNextGeneration){
            // ignore click when cell generation is in process
            return false;
        }

        let initialCellActive = [...this.state.initialCellActive];
        let nextGen = [...this.state.nextGen];

        
        if(nextGen.length === 0){

            // if no nextGen process has happened, then use initialCellActive state with user input
            let cellIndex = initialCellActive.findIndex(rowCol => (rowCol[0] === x && rowCol[1] === y) );

            if(cellIndex >= 0){
                // remove item from array i.e. make cell dead
                initialCellActive.splice(cellIndex, 1);
            } else {
                // add item to array i.e. make cell alive
                initialCellActive.push([x,y]);
            }

            this.setState({ initialCellActive });

        } else {

            // if nextGen process has already happened, use nextGen array to update user input
            let nextGenCell = nextGen.findIndex(item => item.array[0] === x && item.array[1] === y);
            if(nextGenCell >= 0){
                nextGen[nextGenCell].class = (nextGen[nextGenCell].class === "alive" ? "dead" : "alive");
            }

            this.setState({ nextGen });
        }
    }


    getCellClass(x: number, y: number){

        return (this.cellExists(x, y)) ? "alive" : "";
    }

    cellExists(x: number, y: number){

        let isCellExists = false;

        // check if clicked cell exists or not
        if(this.state.initialCellActive.length > 0){
            this.state.initialCellActive.map((item: any) => {
                if(item[0] === x && item[1] === y){
                    isCellExists = true;
                }
            });
        }

        return isCellExists;
    }

    evolveCells(){

        let cellBoard: any[] = [...this.state.cellBoard];
        let nextGen: any[] = [];

        if(cellBoard.length > 0){

            // cellBoard Rows
            cellBoard.map((rowCell, rowIndex) => {

                if(rowCell && rowCell.length > 0) {

                    // cellBoard each Row Columns
                    rowCell.map((colCell: any) => {

                        // get neighbours cells of the current individual cell
                        let neighborCells = this.findingNeighbors(cellBoard, rowIndex, parseInt(colCell.key));

                        let liveNeighborsCount: number = 0;
                        let cellNext: string = "";

                        if(neighborCells.length > 0){

                            // if there are neighbor cells, check the live status of each neighbor cell
                            neighborCells.map((neighborCell: number[]) => {

                                let x = neighborCell[0], y = neighborCell[1] - 1;

                                let cellStatus = cellBoard[x][y];

                                // count live neighbor cells of the individual cell
                                if(cellStatus && cellStatus.props.className.indexOf("alive") >= 0){
                                    liveNeighborsCount++;
                                }
                            });
                        }

                        let elementCell = document.getElementById(colCell.props.id);

                        if(liveNeighborsCount < 2 || liveNeighborsCount > 3){

                            // cell dies
                            cellNext = "dead";

                        } else if(liveNeighborsCount === 3){

                            //cell comes to life
                            cellNext = "alive";

                        } else {

                            //cell stays alive or dead as it is
                            if(elementCell){
                                cellNext = (elementCell.classList.contains("alive") ? "alive" : "dead");
                            }
                        }

                        // creating nextGen cell board
                        nextGen.push({
                            "array": [rowIndex, parseInt(colCell.key)],
                            "class": cellNext
                        });
                    });
                }

            });
        }

        this.setState({ nextGen });
    }

    findingNeighbors(myArray: any[], i: number, j: number) {

        let rowLimit: number = myArray.length-1;
        let columnLimit: number = myArray[1].length;
        let neighborCells: any[] = [];

        for(let x = Math.max(1, i-1); x <= Math.min(i+1, rowLimit); x++) {
            for(let y = Math.max(1, j-1); y <= Math.min(j+1, columnLimit); y++) {
                if(x !== i || y !== j) {
                    neighborCells.push([x, y]);
                }
            }
        }

        return neighborCells;
    }


    render(){

        const {isNextGeneration, cellBoard, nextGen} = this.state;

        // create/recreate cellBoard with initial input or nextGen process
        for(let x = 1; x <= 20; x++){

            let cellCol = [], cellClass = "";

            for(let y = 1; y <= 20; y++){

                if(nextGen.length === 0){
                    cellClass = cellBoard.length > 0 ? this.getCellClass(x, y) : "dead";
                } else {
                    // find class to make alive or dead from nextGen array
                    let nextGenCell = nextGen.find(item => item.array[0] === x && item.array[1] === y);
                    if(nextGenCell && nextGenCell.class){
                        cellClass = nextGenCell.class;
                    }

                }

                cellCol.push(<span className={"cell-block " + cellClass} key={y} id={"cell-" + x + "-" + y} onClick={this.clickCell.bind(this, x, y)}>cell</span>);
            }

            cellBoard[x] = cellCol;
        }


        return (
            <div className="App">
                <h2>Cell Simulator</h2>

                <div className="cell-board">
                    {
                        cellBoard.map((cellX, index) =>
                            <div className="cell-row" key={index}>
                                {cellX}
                            </div>
                        )
                    }
                </div>

                <div className="actions">
                    <span className="button" onClick={this.clickGeneration.bind(this)} >{ (!isNextGeneration ? "Start Next" : "Stop") + " Generation"}</span>
                    <span className="button reset" onClick={this.resetGeneration.bind(this)} >Reset</span>
                </div>

            </div>
        );
    }
}

export default App;
