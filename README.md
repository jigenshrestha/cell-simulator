## Requirements

Please make sure node.js and npm is installed on the machine. Also, make sure typescript is installed as well.

## Installation

Once you have downloaded the project in folder, in the project directory, via command line, you can run:
`npm install` to download required packages/modules

## Run Program

After packages have been installed, you can run: `npm start`

It will run the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
